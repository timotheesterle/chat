$(() => {
    const socket = io();
    $("form").submit(function (e) {
        e.preventDefault();
        socket.emit("chat message", $("#m").val());
        return false;
    });
    socket.on("chat message", msg => {
        $("#messages").append($("<li>").text(msg));
    });
});