const express = require("express");
const app = express();
const http = require("http").createServer(app);
const io = require("socket.io")(http);

app.use(express.static("public"));
app.use('/scripts', express.static(__dirname + '/node_modules/jquery/dist'));
app.use('/scripts', express.static(__dirname + '/node_modules/socket.io-client/dist'));

app.get("/", (req, res) => res.sendFile(`${__dirname}/public/index.html`));

io.on("connection", socket => {
    console.log("a user connected");
    socket.on("disconnect", () => console.log("user disconnected"));
    socket.on("chat message", msg => {
        console.log(`message: ${msg}`);
        io.emit("chat message", msg);
    });
});

http.listen(process.env.PORT || 3000, () => console.log("listening"));

